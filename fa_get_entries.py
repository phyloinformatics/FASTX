#!/usr/bin/env python2.6
# -*- coding: utf-8 -*-

# get_sequences.py
# Read a fasta file and print out selected sequences only

# Import modules and libraries
import sys,re
sys.path.append("/home/dmachado/sw/PYTHON_MODULES/argparse-1.4.0")
import argparse
sys.path.append("/home/dmachado/sw/PYTHON_MODULES/biopython-1.68")
from Bio import SeqIO

# Set arguments
parser=argparse.ArgumentParser()
parser.add_argument("-i","--input",help="Input file in FASTA format",type=str,required=True)
parser.add_argument("-s","--selected",help="Text files with the selected sequence IDs, one per line",type=str,required=True)
args=parser.parse_args()

# Define functions
def main():
	handle=open(args.selected,"r")
	selection=handle.read()
	handle.close()
	selection=re.compile("(>.+)").findall(selection)
	selection=[x.strip() for x in selection]
	with open(args.input,"r") as handle:
		for record in SeqIO.parse(handle,"fasta"):
			id=">{}".format(record.description)
			if(id in selection):
				sys.stdout.write(">{}\n{}\n".format(record.description,record.seq))
			else:
				pass
	return

# Execute functions
main()

# Quit
exit()
