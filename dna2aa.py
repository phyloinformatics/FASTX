#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Name: dna2aa.py
# Description: Reads a multi-FASTA file with protein-coding DNA sequences and returns a multi-FASTA file with the corresponding amino acid sequences (excluding stop codons)
# GitLab: https://gitlab.com/MachadoDJ/FASTX

##
# Import libraries
##

import argparse, sys, re
from Bio import SeqIO
from Bio.Seq import Seq

##
# Define functions
##

def stdout(text):
	return sys.stdout.write("{}\n".format(str(text)))

def stderr(text):
	return sys.stderr.write("{}\n".format(str(text)))

def main():
	with open(args.dna) as handle:
		for record in SeqIO.parse(handle, "fasta"):
			if args.long:
				header = str(record.description)
			else:
				header = str(record.id)
			coding_dna = str(record.seq).upper()
			coding_dna = Seq(coding_dna)
			tdic = {}
			for orientation in [1, -1]:
				for frame in [1, 2, 3]:
					chunk = coding_dna[::orientation]
					chunk = chunk[frame - 1:]
					x = len(chunk)%3
					if x != 0:
						chunk = chunk[:-(len(chunk)%3)]
					translation = str(chunk.translate(table = args.code, to_stop = False))
					lorfs = [lorf for lorf in re.compile("([^\*]+)").findall(translation) if lorf.count("X") <= args.threshold * len(lorf)] # We are ignoring sequences with more than 10% of ambiguous aminoacids
					if lorfs:
						lorf = max(lorfs, key = len)
						tdic[len(lorf)] = [orientation, frame, lorf]
			if len(tdic) >= 1:
				orientation, frame, lorf = tdic[max(tdic)]
				stdout(">{} LORF orientation={} frame={}\n{}".format(header, orientation, frame, lorf))
			else:
				stderr("Could not translate {} (skipping)".format(header))

	return

##
# Execute functions
##

if __name__ == '__main__':
	parser=argparse.ArgumentParser()
	parser.add_argument("-d", "--dna", help = "Path to the multi-FASTA file with protein coding sequences", type = str, required = True)
	parser.add_argument("-c", "--code", help = "Translation table (i.e., NCBI's translation number; default = 1)", type = int, default = 1, required = False)
	parser.add_argument("-t", "--threshold", help = "Maximum accepted frequency of ambiguous amino acids (default = 0.1)", type = float, default = 0.1, required = False)
	parser.add_argument("-l", "--long", help = "Keep full sequence description in headers", action = "store_true")
	args = parser.parse_args()
	main() # This is the main fuction

exit() # Quit this script
