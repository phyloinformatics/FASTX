#!/usr/bin/env python
# -*- coding: utf-8 -*-

# fq2fa.py
# Convert fastq files to fasta format

# Import modules and libraries
import argparse,sys

from Bio import SeqIO

# Set arguments
parser=argparse.ArgumentParser()
parser.add_argument("-i","--input",help="Input in fastq format",type=str,required=True)
parser.add_argument("-o","--output",help="Prefix for output file names",type=str,default="output",required=False)
args=parser.parse_args()

# Define functions
def main():
	for record in SeqIO.parse(args.input,"fastq"):
		entry=">{}\n{}\n".format(record.description,record.seq)
		sys.stdout.write(entry)
	return


# Execute functions
main()

# Quit
exit()
