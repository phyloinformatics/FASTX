#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Name: parseGapsInFasta.py
# Description: Reads a multiple sequence alignment in FASTA format. Replaces indels by missing data according to different parameters.

##
# Import libraries
##

import argparse, re, sys
from Bio import SeqIO

##
# Parse arguments from the command line
##

parser=argparse.ArgumentParser()
parser.add_argument("-c", "--clean", help = "Removes terminals with more gaps or missing data than the specified threshold. Takes values from 0 to 1 (percentage). If set to zero, no terminals will be removed (default = 0.0)", type = float, default = 0.0, required = False)
parser.add_argument("-e", "--ends", help = "Replace trailing gaps (at the ends) by missing data", action = "store_true")
parser.add_argument("-f", "--fasta", help = "Input file in FASTA format", type = str, required = True)
parser.add_argument("-g", "--gap", help = "Symbol for gaps (default = -)", type = str, default = "-", required = False)
parser.add_argument("-l", "--length", help = "Replace internal indels equal to or longer than the predefined length. Takes positive integers. If set to zero, internal indels won't be replaced (default = 0)", type = int, default = 0, required = False)
parser.add_argument("-m", "--missing", help = "Symbol for missing data (default = N)", type = str, default = "N", required = False)
parser.add_argument("-p", "--partitions", help = "Replaces any character composed of only gaps or missing data by a column of pound signs. The default behavior is to remove those columns", action = "store_true")
parser.add_argument("-t", "--trim", help = "Trim ends of the alignment if the amount of missing data is greater than or equal to the threshold. Takes values from 0 to 1 (percentage). If set to zero, the sequences won't be trimmed (default = 0.0)", type = float, default = 0.0, required = False)
parser.add_argument("-v", "--verbose", help = "Increases verbosity", action = "store_true")
args = parser.parse_args()

##
# Define functions
##

def out(string):
	return sys.stdout.write("{}\n".format(str(string).strip()))

def err(string):
	return sys.stderr.write("{}\n".format(str(string).strip()))

def main():
	if args.verbose: err("# Reading from {}".format(args.fasta))
	data = {}
	for record in SeqIO.parse(args.fasta, "fasta"):
		description = str(record.description)
		sequence = str(record.seq)
		if args.verbose: err("## {} ({})".format(description, len(sequence)))
		if args.ends:
			pattern = "^([{0}{1}]*)(.+?)([{0}{1}]*)$".format(args.gap, args.missing)
			left, middle, right = re.compile(pattern, re.I).findall(sequence)[0]
			sequence = "{}{}{}".format(args.missing * len(left), middle, args.missing * len(right))
		if args.length > 0:
			if args.verbose: err("### Replacing indels longer than {} by missing data".format(args.length))
			pattern = "{}+".format(args.gap * args.length)
			matches = re.compile(pattern).findall(sequence)
			for item in set(matches):
				replacement = "{}".format(args.missing * len(item))
				sequence = re.sub(item, replacement, sequence)
		data[description] = sequence
	return data

def trim(data):
	if args.verbose: err("# Trimming alignment ends until amount of missing data or gaps is below {}".format(args.trim))
	positions = {}
	for description in data:
		sequence = data[description]
		for i in range(0, len(sequence)):
			if not i in positions:
				positions[i] = []
			positions[i].append(sequence[i])
	blacklist = []
	carryon = True
	while carryon == True:
		for i in sorted(positions):
			percentage = (float(positions[i].count(args.gap)) + float(positions[i].count(args.missing))) / float(len(positions[i]))
			if percentage >= args.trim:
				blacklist.append(i)
			else:
				carryon = False
				break
	if len(blacklist) >= 1:
		left = int(max(blacklist)) + 1
	else:
		left = 0
	blacklist = []
	carryon = True
	while carryon == True:
		for i in sorted(positions)[::-1]:
			percentage = (float(positions[i].count(args.gap)) + float(positions[i].count(args.missing))) / float(len(positions[i]))
			if percentage >= args.trim:
				blacklist.append(i)
			else:
				carryon = False
				break
	if len(blacklist) >= 1:
		right = int(min(blacklist))
	else:
		right = None
	partitons = []
	for i in sorted(positions):
		if (positions[i].count(args.gap) + positions[i].count(args.missing)) == len(positions[i]):
			partitons.append(i)
	return left, right, partitons

def parseColumns(data, partitions):
	for description in data:
		sequence = data[description]
		for i in sorted(partitions)[::-1]:
			if args.partitions:
				print(">", i)
				sequence = sequence[:i] + "#" + sequence[i+1:]
			else:
				sequence = sequence[:i] + sequence[i+1:]
		data[description] = sequence
	return data

def report(data, left, right):
	if args.verbose: err("# Reporting final alignment (trimmed from position 0 to {} and after {})".format(left, right))
	for description in data:
		sequence = data[description][left : right]
		if args.clean > 0.0:
			if ((float(sequence.count(args.gap)) + float(sequence.count(args.missing))) / float(len(sequence)) > args.clean):
				pass
			else:
				out(">{}\n{}\n".format(description, sequence))
		else:
			out(">{}\n{}\n".format(description, sequence))
	return

##
# Execute functions
##

if args.verbose: err("# This script reads a multiple sequence alignment in FASTA format. Replaces indels by missing data according to different parameters. Use argumen -h or --help for details. The results are printed into the stdout. Verbose mode prints additional text into stderr.")

data = main() # This is the main fuction

if args.trim > 0.0 and args.trim <= 1.0:
	left, right, partitions = trim(data)
else:
	left, right, partitions = 0, None, []

if len(partitions) > 0 :
	data = parseColumns(data, partitions)

report(data, left, right)

exit() # Quit this script
