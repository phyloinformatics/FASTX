#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# filter_protein_db.py
# Parse a multi-FASTA file and removes sequences shorter than a given number of amino acids. This script also removes duplicated sequences and sequences that are substrings of longer sequences. Finally, sequences with the words 'partial' or 'low quality' in their descriptions will be ignored.

##
# Import libraries
##

import argparse, sys, re
from Bio import SeqIO

##
# Define functions
##

def stdout(text):
	return sys.stdout.write("{}\n".format(str(text)))

def stderr(text):
	return sys.stderr.write("{}\n".format(str(text)))

def main():
	fastaDic = {}
	entriesCounter = 0
	with open(args.fasta, "r") as handle:
		for record in SeqIO.parse(handle, "fasta"):
			entriesCounter += 1
			seqId = str(record.id)
			stderr("Stage 1: Reading {}".format(seqId))
			seqDescription = str(record.description)
			seqDescriptionUpper = seqDescription.upper()
			seqSeq = str(record.seq).upper()
			seqLen = len(seqSeq)
			if (seqLen >= args.threshold) and ("PARTIAL" not in seqDescriptionUpper) and ("LOW QUALITY" not in seqDescriptionUpper):
				if args.description:
					fastaDic[seqSeq] = seqDescription
				else:
					fastaDic[seqSeq] = seqId
	fastaLengths = []
	stderr("Stage 2: sorting accepted sequences by length")
	for seqSeq in fastaDic:
		fastaLengths.append([len(seqSeq), seqSeq])
	fastaLengths = sorted(fastaLengths)
	i = len(fastaLengths)
	while i >= 2:
		stderr("Stage 3: removing duplicated substrings ({})".format(len(fastaLengths)))
		l1, s1 = fastaLengths[0]
		fastaLengths = fastaLengths[1:]
		for l2, s2 in fastaLengths:
			if l1 < l2:
				if s1 in s2:
					del fastaDic[s1]
					break
			else:
				break
		i = len(fastaLengths)
	stderr("Stage 4: From {} entries we kept {} unique sequences and will now print them to the standard output".format(entriesCounter, len(fastaDic)))
	for seqSeq in fastaDic:
		stdout(">{}\n{}".format(fastaDic[seqSeq], seqSeq))
	return

##
# Execute functions
##

if __name__ == '__main__':
	parser=argparse.ArgumentParser()
	parser.add_argument("-f", "--fasta", help = "Path to a single multi-FASTA file", type = str)
	parser.add_argument("-t", "--threshold", help = "Minimum number of amino acids in a sequence (default = 100)", type = int, default = 100)
	parser.add_argument("-d", "--description",action="store_true",help="Keep the sequence description (otherwise it will keep only the sequence ID)")
	args = parser.parse_args()
	main() # This is the main fuction

exit() # Quit this script
