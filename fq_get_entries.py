#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

# get_sequences.py
# Read a fastq file and print out selected sequences only

# Import modules and libraries
import sys,re,argparse
from Bio import SeqIO

# Set arguments
parser=argparse.ArgumentParser()
parser.add_argument("-i","--input",help="Input file in FASTQ format",type=str,required=True)
parser.add_argument("-s","--selected",help="Text files with the selected sequence heads (with or without @), one per line",type=str,required=True)
args=parser.parse_args()

# Define functions
def main():
	selection=[]
	handle=open(args.selected,"r")
	for header in handle.readlines():
		header=header.strip()
		if(header[0]=="@"):
			header=header[1:]
		selection+=[header]
	handle.close()
	with open(args.input,"r") as handle:
		for record in SeqIO.parse(handle,"fastq"):
			if(record.id in selection):
				sys.stdout.write("{}".format(record.format("fastq")))
				selection.remove(record.id)
	return

# Execute functions
main()

# Quit
exit()
