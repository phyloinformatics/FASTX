#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# split_fa.py
# Split a fasta sequence file in one file per entry

# Import modules and libraries
import argparse,re

# Set arguments
parser=argparse.ArgumentParser()
parser.add_argument("-i","--input",help="Input (path to multifasta file)",type=str,required=True)
parser.add_argument("-o","--output",help="Output file suffix (default=fasta)",type=str,default="fasta",required=False)
parser.add_argument("-s","--size",help="Maximum number of entries per output fasta file (default=1)",type=int,default=1,required=False)
args=parser.parse_args()

# Define functions
def main():
	handle=open(args.input,"r")
	all=re.compile("(>[^>]+)",re.I|re.M|re.S).findall(handle.read())
	handle.close()
	for entry in all:
		id=re.compile(">([^\s]+)").findall(entry)[0] # Edit this line according to how sequence ID is formated
		handle=open("{}.{}".format(id,args.output),"w")
		handle.write(entry)
		handle.close()
	return

def alt():
	handle=open(args.input,"r")
	all=re.compile("(>[^>]+)",re.I|re.M|re.S).findall(handle.read())
	handle.close()
	count=0
	while True:
		select="\n".join(all[:args.size])
		all=all[args.size:]
		count+=1
		filename="part{}.{}".format(count,args.output)
		handle=open(filename,"w")
		handle.write(select)
		handle.close()
		if(len(all)==0):
			break
	return

# Execute functions
if(args.size==1):
	main()
elif(args.size>=2):
	alt()


# Quit
exit()
