#!/usr/bin/env python
# -*- coding: utf-8 -*-

# get_sequences.py
# Read a fasta file and print out selected sequences only

# Import modules and libraries
import argparse,sys,os
from Bio import SeqIO

# Set arguments
parser=argparse.ArgumentParser()
parser.add_argument("-i","--input",help="Input file in FASTA format",type=str,required=True)
parser.add_argument("-s","--selected",help="Selected entry (ID only)",type=str,required=True)
parser.add_argument("-d","--dir",help="Path to output directory",type=str,default="./output/",required=False)
# For the header "> x y z...", x is the sequence id
args=parser.parse_args()

# Define functions
def main():
	with open(args.input,"r") as handle:
		for record in SeqIO.parse(handle,"fasta"):
			if(record.id==args.selected):
				entry=">{}\n{}\n".format(record.description,str(record.seq))
				filename="{}/{}.fasta".format(args.dir,args.selected)
				if not os.path.exists(os.path.dirname(filename)):
					try:
						os.makedirs(os.path.dirname(filename))
					except OSError as exc: # Guard against race condition
						if exc.errno != errno.EEXIST:
							raise
				with open(filename, "w") as output:
					output.write(entry)
				break
	return

# Execute functions
main()

# Quit
exit()
