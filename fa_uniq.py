#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# fa_uniq.py

# This is a Python3 script that uses Biopython. The script should receive a list of one or more FASTA files as input using argument option "-i" or "--input". For each FASTA file in the input, the script should create an output file with the same name of the original file plus the ".unique" suffix. The script should read a FASTA file and only output its unique sequences into the corresponding output file.

import argparse
from Bio import SeqIO

def filter_unique_sequences(input_files):
    for input_file in input_files:
        output_file = input_file + '.unique'
        unique_sequences = set()
        with open(input_file, 'r') as input_handle, open(output_file, 'w') as output_handle:
            for record in SeqIO.parse(input_handle, 'fasta'):
                if str(record.seq) not in unique_sequences:
                    unique_sequences.add(str(record.seq))
                    SeqIO.write(record, output_handle, 'fasta')

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Filter unique sequences from FASTA files')
    parser.add_argument('-i', '--input', nargs='+', type=str, required=True, help='Input FASTA file(s)')
    args = parser.parse_args()
    input_files = args.input
    filter_unique_sequences(input_files)

exit()