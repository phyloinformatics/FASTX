#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# filter_from_outfmt6.py.py
# Extract query IDs from BlastN (outfmt 6 format) into a new fastq file

# Import modules and libraries
import argparse,re

# Set arguments
parser=argparse.ArgumentParser()
parser.add_argument("-f","--fastq",dest="fq",help="Input sequence file in FASTQ format",type=str,required=True)
parser.add_argument("-o","--outfmt6",dest="o6",help="Hits from Blast in outfmt 6 format",type=str,required=True)
parser.add_argument("-e","--evalue",dest="ev",help="Maximum e-value",type=float,default=1e-5,required=False)
args=parser.parse_args()

# Define functions
def main():
	q=parse_outfmt6(args.o6)
	parse_fq(args.fq,q)
	return

def parse_outfmt6(o6):
	q=[]
	handle=open(o6,"r")
	for l in handle.readlines():
		l=l.strip()
		if(l):
			l=l.split("\t")
			if(float(l[10])<=args.ev):
				q+=[l[0]]
	handle.close()
	return q

def parse_fq(f,q):
	handle=open(f,"r")
	while True:
		l1=handle.readline().strip()
		l2=handle.readline().strip()
		l3=handle.readline().strip()
		l4=handle.readline().strip()
		if not l4:
			break
		else:
			if(l1[1:] in q):
				print("{}\n{}\n{}\n{}\n".format(l1,l2,l3,l4))
	handle.close()
	return

# Execute functions
main()


# Quit
exit()
