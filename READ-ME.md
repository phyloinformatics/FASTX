# FASTX

## Description

This cproject contains a series of scripts to aid with different tasks in manipulating and parsing sequence data (nucleotides or amino acids) from multi-FASTA files.

The scripts are numerous and constantly changing. Execute a script with the argument option `-h` or `--help` to see its usage information. Additional information regarding a script, if any, is commented inside the script.
