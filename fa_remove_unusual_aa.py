#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# fa_remove_unusual_aa
# Reads a multi-FASTA file with amino acid sequences and removes all unusual symbols from the sequences, including gaps and stop codons

##
# Import libraries
##

import argparse, sys, re
from Bio import SeqIO
from Bio.Seq import Seq

##
# Define functions
##

def stdout(text):
	return sys.stdout.write("{}\n".format(str(text)))

def stderr(text):
	return sys.stderr.write("{}\n".format(str(text)))

def main():
	accepted = set([char for char in args.accept])
	with open(args.fasta) as handle:
		for record in SeqIO.parse(handle, "fasta"):
			head = str(record.description).strip()
			seq = str(record.seq).strip()
			if not args.stop:
				if seq[-1] == "*":
					seq = seq[:-1]
				if seq.count("*") > 0:
					stderr("! ERROR: Stop codons found within the sequence {}:\n{}".format(head, seq))
					exit()
			elif seq[:-1].seq.count("*") > 0:
				stderr("! ERROR: Stop codons found within the sequence {}:\n{}".format(head, seq))
				exit()
			myAlphabet = set([char for char in seq])
			if myAlphabet.issubset(accepted):
				pass
			else:
				vilans = list(myAlphabet-accepted)
				stderr("! WARNING: Replacingunauthorized amino acids in {} (found: {}; replaced with X)".format(head, ", ".join(vilans)))
				for vilan in vilans:
					seq = seq.replace(vilan, "X")
			stdout(">{}\n{}".format(head, seq))

	return

##
# Execute functions
##

if __name__ == '__main__':
	parser=argparse.ArgumentParser()
	parser.add_argument("-f", "--fasta", help = "Input multi-FASTA file with amino acid sequences", type = str)
	parser.add_argument("-a", "--accept", help = "String with all characters to be accepted in the amino acid sequences (default = AaRrNnDdCcEeQqGgHhIiLlKkMmFfPpSsTtWwYyVvBbZzJjXx)", type = str, default = "AaRrNnDdCcEeQqGgHhIiLlKkMmFfPpSsTtWwYyVvBbZzJjXx", required = False)
	parser.add_argument("-s", "--stop", help = "Keep stop codons coded as '*' (stop codons will be removed from the end of the sequences by default)", action = "store_true")
	args = parser.parse_args()
	main() # This is the main fuction

exit() # Quit this script
