#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# fqu_simple_check.py
# Check if the fastq files are readable and don't contain errors

import argparse,sys
from Bio import SeqIO

parser=argparse.ArgumentParser()
parser.add_argument("-i","--input",help="Files (sequences in FASTQ format) to be read",type=str,nargs="+",required=True)
parser.add_argument("-v","--verbose", help="Increase output verbosity",action="store_true")
args=parser.parse_args()

def read_all(file):
	count=0
	for record in SeqIO.parse(file,"fastq"):
		count+=1
		if(args.verbose):
			sys.stderr.write("file %s\tid %s\n"%(file,record.id))
	return count

for file in args.input:
	sys.stdout.write("> Reading from file %s\n"%(file))
	count=read_all(file)
	sys.stdout.write("-- %d sequence reads\n"%(count))

exit()
