#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# fa_check_type
# Check if a multi-FASTA file contains only amino acids

##
# Import libraries
##

import argparse, sys, re
from Bio import SeqIO
from Bio.Seq import Seq

##
# Define functions
##

def stdout(text):
	return sys.stdout.write("{}\n".format(str(text)))

def stderr(text):
	return sys.stderr.write("{}\n".format(str(text)))

def main():
	nucleotideAlphabeth = set(["G", "A", "T", "C", "R", "Y", "M", "K", "S", "W", "H", "B", "V", "D", "N", "g", "a", "t", "c", "r", "y", "m", "k", "s", "w", "h", "b", "v", "d", "n"])
	proteinAlphabeth = set(["A", "R", "N", "D", "C", "E", "Q", "G", "H", "O", "I", "L", "K", "M", "F", "P", "U", "S", "T", "W", "Y", "V", "X", "a", "r", "n", "d", "c", "e", "q", "g", "h", "o", "i", "l", "k", "m", "f", "p", "u", "s", "t", "w", "y", "v", "x", "*", "B", "Z", "J", "b", "z", "j"])
	with open(args.fasta) as handle:
		dna = False
		pep = True
		for record in SeqIO.parse(handle, "fasta"):
			mySeq = set([char for char in str(record.seq).strip() if char != ""])
			if mySeq.issubset(nucleotideAlphabeth):
				stderr("DNA: {} looks like a nucleotide sequence".format(record.id))
				if dna == False:
					dna = True
			elif mySeq.issubset(proteinAlphabeth):
				stderr("AA:{} looks like a sequence of amino acids".format(record.id))
				if pep == False:
					pep = True
			else:
				stderr("! ERROR: {} does not look like DNA or AA - these are the unexpected characters\n{}".format(record.id, record.seq))

	if dna and pep:
		stdout("! WARNING: File {} seems to contain a mix of nucleotide and amino acid sequences".format(args.fasta))
	elif dna:
		stdout("File {} seems to contain nucleotide sequences".format(args.fasta))
	elif pep:
		stdout("File {} seems to contain amino acid sequences".format(args.fasta))
	else:
		stderr("! ERROR")
		exit()
	return

##
# Execute functions
##

if __name__ == '__main__':
	parser=argparse.ArgumentParser()
	parser.add_argument("-f", "--fasta", help = "Input multi-FASTA file", type = str)
	args = parser.parse_args()
	main() # This is the main fuction

exit() # Quit this script
